/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.oxoop;

import java.util.Scanner;

/**
 *
 * @author ATICHON
 */
public class Game {

    private Player player1;
    private Player player2;
    private Table table;

    public Game() {
        this.player1 = new Player('O');
        this.player2 = new Player('X');
    }
    
    public void newGame() {
        this.table = new Table(player1, player2);
    }
    
    public void play() {
        showWelcome();
        newGame();
        while(true) {
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()) {
                showTable();
//                saveWin();
                showInfo();
                if(playAgain()) {
                    newGame();
                } else{
                    break;
                }
            }
            if(table.checkDraw()) {
                showTable();
//                saveDraw();
                showInfo();
                if(playAgain()) {
                    newGame();
                } else{
                    break;
                }
            }
            table.switchPlayer();
        }
    }
//    private void saveWin() {
//        if(player1 == table.getCurrentPlayer()) {
//            player1.win();
//            player2.lose();
//        } else {
//            player1.lose();
//            player2.win();
//        }
//    }
//    private void saveDraw() {
//        player1.draw();
//        player2.draw();
//    }
    
    private void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void showTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");  
    }

    public void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Please input row,col: ");
            int row = sc.nextInt();
            int col = sc.nextInt();
            if(table.setRowCol(row, col)) {
                break;
            }
        }
    }

    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);
    }
    
    public boolean playAgain() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Want to play again?(Y/N): ");
        String playAgain = sc.next();
        if(playAgain.equals("Y")) {
            return true;
        } else if(playAgain.equals("y")) {
            return true;
        } else if(playAgain.equals("N")) {
            return false;
        } else if(playAgain.equals("n")) {
            return false;
        }
        return false;
    }
}   